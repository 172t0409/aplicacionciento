package com.example.practicacuento

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_pagina1.*
import kotlinx.android.synthetic.main.activity_pagina2.*

class Pagina1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pagina1)

        val im: Intent = intent;
        var personaje = im.getStringExtra("personaje")
        var texto: String = tvPag1.text.toString()
        var pregunta: String = tvPr1.text.toString()

        if (personaje=="nino"){
            img.setImageResource(R.drawable.boy)
            tvPag1.text = "Habia una vez un niño $texto"
            tvPr1.text = "$pregunta el niño"
        }
        else{
            img.setImageResource(R.drawable.girl)
            tvPag1.text = "Habia una vez una niña $texto"
            tvPr1.text = "$pregunta la niña"
        }

        btnSiguiente.setOnClickListener {
            if (txtNombre.text.toString()!=""){
                val i: Intent = Intent(this, Pagina2::class.java)
                i.putExtra("nombre", txtNombre.text.toString())
                i.putExtra("personaje", personaje)
                startActivity(i)
                finish()
            }
            else
                Toast.makeText(this,"Debe llenar el campo para continuar", Toast.LENGTH_LONG).show()

        }

        btnRegresar.setOnClickListener {
            val i: Intent = Intent(this, MainActivity::class.java)
            startActivity(i)
            finish()
        }
    }
}
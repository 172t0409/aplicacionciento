package com.example.practicacuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_pagina1.*
import kotlinx.android.synthetic.main.activity_pagina3.*

class Pagina3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pagina3)

        val i: Intent = intent;
        var personaje = i.getStringExtra("personaje")
        var lugar : String? = i.getStringExtra("lugar")
        var txt3: String = tvPag3.text.toString();

        if (personaje=="nino")
            tvPag3.text = "El niño $txt3 $lugar"
        else
            tvPag3.text = "La niña $txt3 $lugar"

        btnSiguiente3.setOnClickListener {
            val i2: Intent = Intent(this, Pagina4::class.java)
            startActivity(i2)
            finish()
        }

        btnRegresar3.setOnClickListener {
            val i: Intent = Intent(this, btnRegresar3::class.java)
            startActivity(i)
            finish()
        }

    }
}
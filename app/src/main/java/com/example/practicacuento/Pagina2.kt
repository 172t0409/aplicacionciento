package com.example.practicacuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_pagina1.*
import kotlinx.android.synthetic.main.activity_pagina2.*

class Pagina2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pagina2)

        val i: Intent = intent;
        var nombre = i.getStringExtra("nombre")
        var personaje: String = i.getStringExtra("personaje")
        var texto: String = tvTexto.text.toString()
        tvTexto.text = "Un dia $nombre, $texto "

        btnSiguiente2.setOnClickListener {
            if (txtLugar.text.toString()!=""){
                val i: Intent = Intent(this, Pagina3::class.java)
                i.putExtra("personaje", personaje)
                i.putExtra("lugar", txtLugar.text.toString())
                startActivity(i)
                finish()
            }
            else
                Toast.makeText(this,"Debe llenar el campo para continuar", Toast.LENGTH_LONG).show()

        }

        btnRegresar2.setOnClickListener {
            val i: Intent = Intent(this, Pagina1::class.java)
            startActivity(i)
            finish()
        }

    }
}
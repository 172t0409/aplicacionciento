package com.example.practicacuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_pagina1.*
import kotlinx.android.synthetic.main.activity_pagina4.*

class Pagina4 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pagina4)

        btnSiguiente4.setOnClickListener {
            val i: Intent = Intent(this, Final::class.java)
            startActivity(i)
            finish()
        }

        btnRegresar4.setOnClickListener {
            val i: Intent = Intent(this, Pagina3::class.java)
            startActivity(i)
        }
    }
}
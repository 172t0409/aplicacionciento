package com.example.practicacuento

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_elegir_personaje.*
import kotlinx.android.synthetic.main.activity_pagina1.*

class ElegirPersonaje : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_elegir_personaje)
        var personaje:String
        btnNina.setOnClickListener {
            personaje = "nina";
            val i: Intent = Intent(this, Pagina1::class.java)
            i.putExtra("personaje", personaje )
            startActivity(i)
            finish()
        }

        btnNino.setOnClickListener {
            personaje = "nino";
            val i: Intent = Intent(this, Pagina1::class.java)
            i.putExtra("personaje", personaje )
            startActivity(i)
            finish()
        }
    }
}